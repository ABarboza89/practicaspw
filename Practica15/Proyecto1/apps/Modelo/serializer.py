from rest_framework import serializers
from apps.Conejos.models import Conejo
from django.contrib.auth.models import User

class ApiUser(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
		]
			

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Conejo
		fields = [
			"nombre",
			"raza",
			"edad",
			"size",
			"date",
		]
			
			