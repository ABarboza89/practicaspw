from django.shortcuts import render,redirect
from apps.Lobos.models import Lobo
from apps.Lobos.forms import LoboForm
from django.views.generic import ListView,CreateView,DetailView,UpdateView,DeleteView
from django.urls import reverse_lazy
# Create your views here.

#Generics Views
class gLista(ListView):
	model = Lobo
	template_name ='Lobos/glista.html'

class gDetalles(DetailView):
	model  = Lobo
	template_name = 'Lobos/detalles.html'

class gCrear(CreateView):
	model = Lobo
	form_class = LoboForm
	template_name = 'Lobos/crear.html'
	success_url = reverse_lazy('Lobos:gLista')

class gActualizar(UpdateView):
	model = Lobo
	form_class = LoboForm
	template_name = 'Lobos/crear.html'
	success_url = reverse_lazy('Lobos:gLista')

class gEliminar(DeleteView):
	model  = Lobo
	template_name = 'Lobos/geliminar.html'
	success_url = reverse_lazy('Lobos:gLista')
	
#Funciones 
def Lista(request):
	qs = Lobo.objects.all()
	context ={
		'lobos':qs
	}
	return render(request,'Lobos/lista.html',context)

def Detalles(request,id):
	qs = Lobo.objects.get(id=id)
	context ={
		'lobo':qs
	}
	return render(request,'Lobos/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = LoboForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Lobos:Lista')
	else:
		form = LoboForm()

	return render(request,'Lobos/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Lobo.objects.get(id=id)
	if request.method == 'GET':
		form = LoboForm(instance = qs)
	else:
		form = LoboForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Lobos:Lista')
	return render(request,'Lobos/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Lobo.objects.get(id=id)
	qs.delete()
	return redirect('Lobos:Lista')