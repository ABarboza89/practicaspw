from django.shortcuts import render,redirect
from apps.Aguilas.models import Aguila
from apps.Aguilas.forms import AguilaForm
from django.views.generic import ListView,CreateView,DetailView,UpdateView,DeleteView
from django.urls import reverse_lazy
# Create your views here.

#Generics Views
class gLista(ListView):
	model = Aguila
	template_name ='Aguilas/glista.html'

class gDetalles(DetailView):
	model  = Aguila
	template_name = 'Aguilas/detalles.html'

class gCrear(CreateView):
	model = Aguila
	form_class = AguilaForm
	template_name = 'Aguilas/crear.html'
	success_url = reverse_lazy('Aguilas:gLista')

class gActualizar(UpdateView):
	model = Aguila
	form_class = AguilaForm
	template_name = 'Aguilas/crear.html'
	success_url = reverse_lazy('Aguilas:gLista')

class gEliminar(DeleteView):
	model  = Aguila
	template_name = 'Aguilas/geliminar.html'
	success_url = reverse_lazy('Aguilas:gLista')
	
#Funciones 
def Lista(request):
	qs = Aguila.objects.all()
	context ={
		'aguilas':qs
	}
	return render(request,'Aguilas/lista.html',context)

def Detalles(request,id):
	qs = Aguila.objects.get(id=id)
	context ={
		'aguila':qs
	}
	return render(request,'Aguilas/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = AguilaForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Aguilas:Lista')
	else:
		form = AguilaForm()

	return render(request,'Aguilas/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Aguila.objects.get(id=id)
	if request.method == 'GET':
		form = AguilaForm(instance = qs)
	else:
		form = AguilaForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Aguilas:Lista')
	return render(request,'Aguilas/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Aguila.objects.get(id=id)
	qs.delete()
	return redirect('Aguilas:Lista')