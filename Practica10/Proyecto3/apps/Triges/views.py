from django.shortcuts import render,redirect
from apps.Triges.models import Tigre
from apps.Triges.forms import TigreForm
from django.views.generic import ListView,CreateView,DetailView,UpdateView,DeleteView
from django.urls import reverse_lazy
# Create your views here.

#Generics Views
class gLista(ListView):
	model = Tigre
	template_name ='Tigres/glista.html'

class gDetalles(DetailView):
	model  = Tigre
	template_name = 'Tigres/detalles.html'

class gCrear(CreateView):
	model = Tigre
	form_class = TigreForm
	template_name = 'Tigres/crear.html'
	success_url = reverse_lazy('Triges:gLista')

class gActualizar(UpdateView):
	model = Tigre
	form_class = TigreForm
	template_name = 'Tigres/crear.html'
	success_url = reverse_lazy('Triges:gLista')

class gEliminar(DeleteView):
	model  = Tigre
	template_name = 'Tigres/geliminar.html'
	success_url = reverse_lazy('Triges:gLista')
	
#Funciones 
def Lista(request):
	qs = Tigre.objects.all()
	context = {
		'tigres' : qs
	}
	return render(request,'Tigres/lista.html',context)

def Detalles(request,id):
	qs = Tigre.objects.get(id=id)
	context = {
		'tigre' : qs
	}
	return render(request,'Tigres/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = TigreForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Triges:Lista')
	else:
		form = TigreForm()

	return render(request,'Tigres/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Tigre.objects.get(id=id)
	if request.method == 'GET':
		form = TigreForm(instance = qs)
	else:
		form = TigreForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Triges:Lista')
	return render(request,'Tigres/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Tigre.objects.get(id=id)
	qs.delete()
	return redirect('Triges:Lista')