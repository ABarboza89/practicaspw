from django import forms

from apps.Zorros.models import Zorro

class ZorroForm(forms.ModelForm):
	nombre = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Nombre","class":"form-control"}))
	raza = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Raza","class":"form-control"}))
	hogar = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Raza","class":"form-control"}))
	habitad = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Raza","class":"form-control"}))
	edad = forms.IntegerField(widget=forms.NumberInput(attrs={"placeholder":"Size","class":"form-control"}))
	size = forms.FloatField(widget=forms.TextInput(attrs={"placeholder":"Size","class":"form-control"}))
	date = forms.DateField(widget=forms.DateInput(attrs={"placeholder":"Registro","class":"form-control"}))
	class Meta:
		model = Zorro
		fields = [
			"nombre",
			"raza",
			"hogar",
			"habitad",
			"edad",
			"size",
			"date",

		]
			