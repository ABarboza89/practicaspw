from django.shortcuts import render,redirect
from apps.Zorros.models import Zorro
from apps.Zorros.forms import ZorroForm
from django.views.generic import ListView,CreateView,DetailView,UpdateView,DeleteView
from django.urls import reverse_lazy
# Create your views here.

#Generics Views
class gLista(ListView):
	model = Zorro
	template_name ='Zorro/glista.html'

class gDetalles(DetailView):
	model  = Zorro
	template_name = 'Zorro/detalles.html'

class gCrear(CreateView):
	model = Zorro
	form_class = ZorroForm
	template_name = 'Zorro/crear.html'
	success_url = reverse_lazy('Zorros:gLista')

class gActualizar(UpdateView):
	model = Zorro
	form_class = ZorroForm
	template_name = 'Zorro/crear.html'
	success_url = reverse_lazy('Zorros:gLista')

class gEliminar(DeleteView):
	model  = Zorro
	template_name = 'Zorro/geliminar.html'
	success_url = reverse_lazy('Zorros:gLista')
	
#Funciones 
def Lista(request):
	qs = Zorro.objects.all()
	context = {
		'zorros': qs
	}
	return render(request,'Zorro/lista.html',context)

def Detalles(request,id):
	qs = Zorro.objects.get(id=id)
	context = {
		'zorro': qs
	}
	return render(request,'Zorro/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = ZorroForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Zorros:Lista')
	else:
		form = ZorroForm()

	return render(request,'Zorro/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Zorro.objects.get(id=id)
	if request.method == 'GET':
		form = ZorroForm(instance = qs)
	else:
		form = ZorroForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Zorros:Lista')
	return render(request,'Zorro/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Zorro.objects.get(id=id)
	qs.delete()
	return redirect('Zorros:Lista')