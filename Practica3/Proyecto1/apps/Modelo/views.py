from django.shortcuts import render

# Create your views here.
def Template1(request):
    context = {
        'message': "Soy el Template 1 - Proyecto 1",
    }
    return render(request, 'Modelo/template1.html', context)

def Template2(request):
    context = {
        'message': "Soy el Template 2 - Proyecto 1",
    }
    return render(request, 'Modelo/template2.html', context)

def Template3(request):
    context = {
        'message': "Soy el Template 3 - Proyecto 1",
    }
    return render(request, 'Modelo/template3.html', context)

def Template4(request):
    context = {
        'message': "Soy el Template 4 - Proyecto 1",
    }
    return render(request, 'Modelo/template4.html', context)

def Template5(request):
    context = {
        'message': "Soy el Template 5 - Proyecto 1",
    }
    return render(request, 'Modelo/template5.html', context)

def Template6(request):
    context = {
        'message': "Soy el Template 6 - Proyecto 1",
    }
    return render(request, 'Modelo/template6.html', context)

def Template7(request):
    context = {
        'message': "Soy el Template 7 - Proyecto 1",
    }
    return render(request, 'Modelo/template7.html', context)

def Template8(request):
    context = {
        'message': "Soy el Template 8 - Proyecto 1",
    }
    return render(request, 'Modelo/template8.html', context)

def Template9(request):
    context = {
        'message': "Soy el Template 9 - Proyecto 1",
    }
    return render(request, 'Modelo/template9.html', context)

def Template10(request):
    context = {
        'message': "Soy el Template 10 - Proyecto 1",
    }
    return render(request, 'Modelo/template10.html', context)