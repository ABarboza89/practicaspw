from rest_framework import serializers
from apps.Conejos.models import Conejo

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Conejo
		fields = [
			"nombre",
			"raza",
			"edad",
			"size",
			"date",
		]
			
			