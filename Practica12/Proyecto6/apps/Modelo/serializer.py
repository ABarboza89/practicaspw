from rest_framework import serializers
from apps.Aguilas.models import Aguila

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Aguila
		fields = [
			"nombre",
			"raza",
			"habitad",
			"color",
			"edad",
			"velocidad",
			"size",
			"sizeAlas",
			"peso",
			"date",

		]
			
			