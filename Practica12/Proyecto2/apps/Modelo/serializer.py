from rest_framework import serializers
from apps.Zorros.models import Zorro

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Zorro
		fields = [
			"nombre",
			"raza",
			"hogar",
			"habitad",
			"edad",
			"size",
			"date",

		]
			