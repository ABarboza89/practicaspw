from rest_framework import serializers
from apps.Dragones.models import Dragon
from django.contrib.auth.models import User

class ApiUser(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
		]
			

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Dragon
		fields = [
			"nombre",
			"raza",
			"color",
			"edad",
			"peso",
			"date",
		]
			
			