from django.contrib import admin
from django.urls import path
from apps.Dragones.views import Lista,Detalles,Crear,Actualizar,Eliminar
from apps.Dragones.views import gLista,gDetalles,gCrear,gEliminar,gActualizar

app_name='Dragones'
urlpatterns = [
    path('glista/', gLista.as_view(), name="gLista"),
    path('gdetalles/<int:pk>', gDetalles.as_view(), name="gDetalles"),
    path('gcrear/', gCrear.as_view(), name="gCrear"),
    path('geliminar/<int:pk>', gEliminar.as_view(), name="gEliminar"),
    path('gactualizar/<int:pk>', gActualizar.as_view(), name="gActualizar"),

    path('lista/',Lista,name='Lista'),
    path('detalles/<int:id>',Detalles,name='Detalles'),
    path('crear/',Crear,name='Crear'),
    path('actualizar/<int:id>',Actualizar,name='Actualizar'),
    path('eliminar/<int:id>',Eliminar,name='Eliminar'),
]