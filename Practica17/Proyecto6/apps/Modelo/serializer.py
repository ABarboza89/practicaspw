from rest_framework import serializers
from apps.Aguilas.models import Aguila
from django.contrib.auth.models import User

class ApiUser(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
		]
		
class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Aguila
		fields = [
			"nombre",
			"raza",
			"habitad",
			"color",
			"edad",
			"velocidad",
			"size",
			"sizeAlas",
			"peso",
			"date",

		]
			
			