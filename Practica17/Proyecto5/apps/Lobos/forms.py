from django import forms

from apps.Lobos.models import Lobo

class LoboForm(forms.ModelForm):
	nombre = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Nombre","class":"form-control"}))
	raza = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Raza","class":"form-control"}))
	habitad = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"hogar","class":"form-control"}))
	color = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"color","class":"form-control"}))
	edad = forms.IntegerField(widget=forms.NumberInput(attrs={"placeholder":"edad","class":"form-control"}))
	size = forms.FloatField(widget=forms.TextInput(attrs={"placeholder":"Size","class":"form-control"}))
	peso = forms.FloatField(widget=forms.TextInput(attrs={"placeholder":"peso","class":"form-control"}))
	date = forms.DateField(widget=forms.DateInput(attrs={"placeholder":"Registro","class":"form-control"}))
	class Meta:
		model = Lobo
		fields = [
			"nombre",
			"raza",
			"habitad",
			"color",
			"edad",
			"size",
			"peso",
			"date",

		]
			