from django.shortcuts import render,redirect
from apps.Caballos.models import Caballo
from apps.Caballos.forms import CaballoForm
from django.views.generic import ListView,CreateView,DetailView,UpdateView,DeleteView
from django.urls import reverse_lazy
# Create your views here.

#Generics Views
class gLista(ListView):
	model = Caballo
	template_name ='Caballo/glista.html'

class gDetalles(DetailView):
	model  = Caballo
	template_name = 'Caballo/detalles.html'

class gCrear(CreateView):
	model = Caballo
	form_class = CaballoForm
	template_name = 'Caballo/crear.html'
	success_url = reverse_lazy('Caballos:gLista')

class gActualizar(UpdateView):
	model = Caballo
	form_class = CaballoForm
	template_name = 'Caballo/crear.html'
	success_url = reverse_lazy('Caballos:gLista')

class gEliminar(DeleteView):
	model  = Caballo
	template_name = 'Caballo/geliminar.html'
	success_url = reverse_lazy('Caballos:gLista')


def Lista(request):
	qs = Caballo.objects.all()
	context = {
		'caballos': qs
	}
	return render(request,'Caballo/lista.html',context)

def Detalles(request,id):
	qs = Caballo.objects.get(id=id)
	context = {
		'caballo': qs
	}
	return render(request,'Caballo/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = CaballoForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Caballo:Lista')
	else:
		form = CaballoForm()

	return render(request,'Caballo/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Caballo.objects.get(id=id)
	if request.method == 'GET':
		form = CaballoForm(instance = qs)
	else:
		form = CaballoForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Caballo:Lista')
	return render(request,'Caballo/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Caballo.objects.get(id=id)
	qs.delete()
	return redirect('Caballo:Lista')