from django.shortcuts import render
from apps.Dragones.models import Dragon
from apps.Dragones.forms import DragonForm
# Create your views here.
def Lista(request):
	qs = Dragon.objects.all()
	context ={
		'dragones':qs
	}
	return render(request,'Dragones/lista.html',context)

def Detalles(request,id):
	qs = Dragon.objects.get(id=id)
	context ={
		'dragon':qs
	}
	return render(request,'Dragones/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = DragonForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Dragones:Lista')
	else:
		form = DragonForm()

	return render(request,'Dragones/crear.html',{'form':form})