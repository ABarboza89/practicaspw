from django.shortcuts import render
from apps.Caballos.models import Caballo
from apps.Caballos.forms import CaballoForm
# Create your views here.
def Lista(request):
	qs = Caballo.objects.all()
	context = {
		'caballos': qs
	}
	return render(request,'Caballo/lista.html',context)

def Detalles(request,id):
	qs = Caballo.objects.get(id=id)
	context = {
		'caballo': qs
	}
	return render(request,'Caballo/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = CaballoForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Caballo:Lista')
	else:
		form = CaballoForm()

	return render(request,'Caballo/crear.html',{'form':form})