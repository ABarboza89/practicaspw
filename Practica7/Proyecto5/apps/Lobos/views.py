from django.shortcuts import render
from apps.Lobos.models import Lobo
from apps.Lobos.forms import LoboForm
# Create your views here.
def Lista(request):
	qs = Lobo.objects.all()
	context ={
		'lobos':qs
	}
	return render(request,'Lobos/lista.html',context)

def Detalles(request,id):
	qs = Lobo.objects.get(id=id)
	context ={
		'lobo':qs
	}
	return render(request,'Lobos/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = LoboForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Lobos:Lista')
	else:
		form = LoboForm()

	return render(request,'Lobos/crear.html',{'form':form})