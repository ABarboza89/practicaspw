from django.shortcuts import render,redirect
from apps.Conejos.models import Conejo
from apps.Conejos.forms import ConejoForm

# Create your views here.
def Lista(request):
	qs = Conejo.objects.all()	
	context = {
		"conejos": qs
	}
	return render(request,"Conejo/lista.html",context)

def Detalles(request,id):
	qs = Conejo.objects.get(id=id)	
	context = {
		"conejo": qs
	}
	return render(request,"Conejo/detalles.html",context)

def Crear(request):
	if request.method == 'POST':
		form = ConejoForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Conejos:Lista')
	else:
		form = ConejoForm()

	return render(request,'Conejo/crear.html',{'form':form})