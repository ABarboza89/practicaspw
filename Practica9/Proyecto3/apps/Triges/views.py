from django.shortcuts import render
from apps.Triges.models import Tigre
from apps.Triges.forms import TigreForm
# Create your views here.
def Lista(request):
	qs = Tigre.objects.all()
	context = {
		'tigres' : qs
	}
	return render(request,'Tigres/lista.html',context)

def Detalles(request,id):
	qs = Tigre.objects.get(id=id)
	context = {
		'tigre' : qs
	}
	return render(request,'Tigres/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = TigreForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Triges:Lista')
	else:
		form = TigreForm()

	return render(request,'Tigres/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Tigre.objects.get(id=id)
	if request.method == 'GET':
		form = TigreForm(instance = qs)
	else:
		form = TigreForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Triges:Lista')
	return render(request,'Tigres/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Tigre.objects.get(id=id)
	qs.delete()
	return redirect('Triges:Lista')