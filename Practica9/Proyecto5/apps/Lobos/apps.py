from django.apps import AppConfig


class LobosConfig(AppConfig):
    name = 'Lobos'
