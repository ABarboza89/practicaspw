from django.contrib import admin
from django.urls import path
from apps.Gatos.views import Lista,Detalles,Crear,Actualizar,Eliminar

app_name='Gatos'
urlpatterns = [
    path('lista/',Lista,name='Lista'),
    path('detalles/<int:id>',Detalles,name='Detalles'),
    path('crear/',Crear,name='Crear'),
    path('actualizar/<int:id>',Actualizar,name='Actualizar'),
    path('eliminar/<int:id>',Eliminar,name='Eliminar'),
]