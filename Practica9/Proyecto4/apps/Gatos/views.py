from django.shortcuts import render
from apps.Gatos.models import Gato
from apps.Gatos.forms import GatoForm
# Create your views here.
def Lista(request):
	qs = Gato.objects.all()
	context ={
		'gatos':qs
	}
	return render(request,'Gatos/lista.html',context)

def Detalles(request,id):
	qs = Gato.objects.get(id=id)
	context ={
		'gato':qs
	}
	return render(request,'Gatos/detalles.html',context)


def Crear(request):
	if request.method == 'POST':
		form = GatoForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Gatos:Lista')
	else:
		form = GatoForm()

	return render(request,'Gatos/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Gato.objects.get(id=id)
	if request.method == 'GET':
		form = GatoForm(instance = qs)
	else:
		form = GatoForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Gatos:Lista')
	return render(request,'Gatos/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Gato.objects.get(id=id)
	qs.delete()
	return redirect('Gatos:Lista')