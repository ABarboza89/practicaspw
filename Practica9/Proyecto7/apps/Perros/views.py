from django.shortcuts import render
from apps.Perros.models import Perro
from apps.Perros.forms import PerroForm
# Create your views here.
def Lista(request):
	qs = Perro.objects.all()
	context ={
		'perros':qs
	}
	return render(request,'Perros/lista.html',context)

def Detalles(request,id):
	qs = Perro.objects.get(id=id)
	context ={
		'perro':qs
	}
	return render(request,'Perros/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = PerroForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Perros:Lista')
	else:
		form = PerroForm()

	return render(request,'Perros/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Perro.objects.get(id=id)
	if request.method == 'GET':
		form = PerroForm(instance = qs)
	else:
		form = PerroForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Perros:Lista')
	return render(request,'Perros/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Perro.objects.get(id=id)
	qs.delete()
	return redirect('Perros:Lista')