from django.shortcuts import render,redirect
from apps.Patos.models import Pato
from apps.Patos.forms import PatoForm

from django.views.generic import ListView,CreateView,DetailView,UpdateView,DeleteView
from django.urls import reverse_lazy
# Create your views here.

#Generics Views
class gLista(ListView):
	model = Pato
	template_name ='Patos/glista.html'

class gDetalles(DetailView):
	model  = Pato
	template_name = 'Patos/detalles.html'

class gCrear(CreateView):
	model = Pato
	form_class = PatoForm
	template_name = 'Patos/crear.html'
	success_url = reverse_lazy('Patos:gLista')

class gActualizar(UpdateView):
	model = Pato
	form_class = PatoForm
	template_name = 'Patos/crear.html'
	success_url = reverse_lazy('Patos:gLista')

class gEliminar(DeleteView):
	model  = Pato
	template_name = 'Patos/geliminar.html'
	success_url = reverse_lazy('Patos:gLista')
	
#Funciones 
def Lista(request):
	qs = Pato.objects.all()
	context ={
		'patos':qs
	}
	return render(request,'Patos/lista.html',context)

def Detalles(request,id):
	qs = Pato.objects.get(id=id)
	context ={
		'pato':qs
	}
	return render(request,'Patos/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = PatoForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Patos:Lista')
	else:
		form = PatoForm()

	return render(request,'Patos/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Pato.objects.get(id=id)
	if request.method == 'GET':
		form = PatoForm(instance = qs)
	else:
		form = PatoForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Patos:Lista')
	return render(request,'Patos/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Pato.objects.get(id=id)
	qs.delete()
	return redirect('Patos:Lista')