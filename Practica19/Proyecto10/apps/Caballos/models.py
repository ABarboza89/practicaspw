from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
class Caballo(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)
	hogar = models.CharField(max_length=30)	
	edad = models.IntegerField()
	altura = models.FloatField()
	ancho = models.FloatField()
	date = models.DateField()

	def __str__(self):
		return self.nombre

	def clean(self,*args,**kwargs):
		raza = self.raza
		raza_list = ["ARABE","MILLA","FRISON","CLYDESDALE","MUSTANG","PURASANGRE"]
		if raza.upper() not in raza_list:
			raise ValidationError("Tipos de caballos [Arabe,Milla,Frison,Clydesdale,Mustang,Purasangre]")
		return super(Caballo,self).clean(*args,**kwargs)