from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
class Conejo(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)	
	edad = models.IntegerField()
	size = models.IntegerField()
	date = models.DateField()

	def __str__(self):
		return self.nombre

	def clean(self,*args,**kwargs):
		raza = self.raza
		raza_list = ["SILVER","COMUN","ANGORA","BELIER HOLANDES","HOLANDES ENANO"]
		if raza.upper() not in raza_list:
			raise ValidationError("Tipos de conjeos [Silver,Comun,Angora,Belier Holandes,Holandes Enano]")
		return super(Conejo,self).clean(*args,**kwargs)