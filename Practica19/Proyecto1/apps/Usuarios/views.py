from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import CreateView,DetailView,UpdateView
from django.urls import reverse_lazy
from apps.Usuarios.forms import RegistroForm

# Create your views here.
class UsuarioRegistro(CreateView):
	model = User
	template_name = "Conejo/crear.html"
	form_class = RegistroForm
	success_url = reverse_lazy('Modelo:Index')

		