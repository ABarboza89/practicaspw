from django.db import models

from django.core.exceptions import ValidationError
# Create your models here.
class Aguila(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)
	habitad = models.CharField(max_length=30)	
	color = models.CharField(max_length=30)
	edad = models.IntegerField()
	velocidad  = models.FloatField()
	size = models.FloatField()
	sizeAlas = models.FloatField()
	peso = models.FloatField()
	date = models.DateField()

	def __str__(self):
		return self.nombre

	def clean(self,*args,**kwargs):
		raza = self.raza
		raza_list = ["REAL","ARPIA","CORONADA","AMERICANA","BLANCA"]
		if raza.upper() not in raza_list:
			raise ValidationError("Tipos de Aguilas [Real,Arpia,Coronada,Americana,Blanca]")
		return super(Lobo,self).clean(*args,**kwargs)