from rest_framework import serializers
from apps.Gatos.models import Gato
from django.contrib.auth.models import User

class ApiUser(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
		]
		
class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Gato
		fields = [
			"nombre",
			"raza",
			"hogar",
			"color",
			"edad",
			"size",
			"peso",
			"date",

		]
			
			