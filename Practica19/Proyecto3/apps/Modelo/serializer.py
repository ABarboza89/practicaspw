from rest_framework import serializers
from apps.Triges.models import Tigre
from django.contrib.auth.models import User

class ApiUser(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
		]
			

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Tigre
		fields = [
			"nombre",
			"raza",
			"hogar",
			"habitad",
			"color",
			"edad",
			"size",
			"peso",
			"date",

		]
			
			