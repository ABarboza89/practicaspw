from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
class Tigre(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)
	hogar = models.CharField(max_length=30)	
	habitad = models.CharField(max_length=30)
	color = models.CharField(max_length=30)
	edad = models.IntegerField()
	size = models.FloatField()
	peso = models.FloatField()
	date = models.DateField()

	def __str__(self):
		return self.nombre

	def clean(self,*args,**kwargs):
		raza = self.raza
		raza_list = ["BLANCO","DORADO","AZUL","BENGALA","MALAYA","AMUR"]
		if raza.upper() not in raza_list:
			raise ValidationError("Tipos de tigres [Blanco,Dorado,Azul,Bengala,Malaya,Amur]")
		return super(Trige,self).clean(*args,**kwargs)