from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
class Lobo(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)
	habitad = models.CharField(max_length=30)	
	color = models.CharField(max_length=30)
	edad = models.IntegerField()
	size = models.FloatField()
	peso = models.FloatField()
	date = models.DateField()

	def __str__(self):
		return self.nombre

	def clean(self,*args,**kwargs):
		raza = self.raza
		raza_list = ["GRIS","SIBERIANO","BLANCO","MEXICANO","BAFFIN","YUKON"]
		if raza.upper() not in raza_list:
			raise ValidationError("Tipos de Lobos [Gris,Siberiano,Blanco,Mexicano,Baffin,Yukon]")
		return super(Lobo,self).clean(*args,**kwargs)