from rest_framework import serializers
from apps.Lobos.models import Lobo
from django.contrib.auth.models import User

class ApiUser(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
		]
			

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Lobo
		fields = [
			"nombre",
			"raza",
			"habitad",
			"color",
			"edad",
			"size",
			"peso",
			"date",

		]
		
			