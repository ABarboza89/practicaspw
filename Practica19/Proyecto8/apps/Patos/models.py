from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
class Pato(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)
	habitad = models.CharField(max_length=30)	
	color = models.CharField(max_length=30)
	edad = models.IntegerField()
	peso = models.FloatField()
	date = models.DateField()

	def __str__(self):
		return self.nombre


	def clean(self,*args,**kwargs):
		raza = self.raza
		raza_list = ["AMERICANO","CORREDOR","DOMESTICO","CAYUNA","ROUNE","GRASS"]
		if raza.upper() not in raza_list:
			raise ValidationError("Tipos de Patos [Americano,Corredor,Domestico,Cayuna,Roune,Grass]")
		return super(Pato,self).clean(*args,**kwargs)