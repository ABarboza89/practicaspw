from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
class Dragon(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)
	color = models.CharField(max_length=30)
	edad = models.IntegerField()
	peso = models.FloatField()
	date = models.DateField()

	def __str__(self):
		return self.nombre

	def clean(self,*args,**kwargs):
		raza = self.raza
		raza_list = ["FUEGO","AGUA","TIERRA","VIENTO","OSCURO","DIOS","FUEGO OSCURO","ELECTRICO","VENENO"]
		if raza.upper() not in raza_list:
			raise ValidationError("Tipos de Dragones [Fuego,Agua,Tierra,Viento,Oscuro,Dios,Fuego Oscuro,Electrico,Veneno")
		return super(Lobo,self).clean(*args,**kwargs)