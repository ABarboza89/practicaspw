from rest_framework import serializers
from apps.Gatos.models import Gato

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Gato
		fields = [
			"nombre",
			"raza",
			"hogar",
			"color",
			"edad",
			"size",
			"peso",
			"date",

		]
			
			