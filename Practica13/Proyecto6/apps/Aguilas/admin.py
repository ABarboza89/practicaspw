from django.contrib import admin

from apps.Aguilas.models import Aguila

# Register your models here.
admin.site.register(Aguila)
