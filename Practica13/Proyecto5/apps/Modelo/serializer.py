from rest_framework import serializers
from apps.Lobos.models import Lobo

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Lobo
		fields = [
			"nombre",
			"raza",
			"habitad",
			"color",
			"edad",
			"size",
			"peso",
			"date",

		]
		
			