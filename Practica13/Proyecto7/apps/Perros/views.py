from django.shortcuts import render,redirect
from apps.Perros.models import Perro
from apps.Perros.forms import PerroForm
from django.views.generic import ListView,CreateView,DetailView,UpdateView,DeleteView
from django.urls import reverse_lazy
# Create your views here.

#Generics Views
class gLista(ListView):
	model = Perro
	template_name ='Perros/glista.html'

class gDetalles(DetailView):
	model  = Perro
	template_name = 'Perros/detalles.html'

class gCrear(CreateView):
	model = Perro
	form_class = PerroForm
	template_name = 'Perros/crear.html'
	success_url = reverse_lazy('Perros:gLista')

class gActualizar(UpdateView):
	model = Perro
	form_class = PerroForm
	template_name = 'Perros/crear.html'
	success_url = reverse_lazy('Perros:gLista')

class gEliminar(DeleteView):
	model  = Perro
	template_name = 'Perros/geliminar.html'
	success_url = reverse_lazy('Perros:gLista')
	
#Funciones 
def Lista(request):
	qs = Perro.objects.all()
	context ={
		'perros':qs
	}
	return render(request,'Perros/lista.html',context)

def Detalles(request,id):
	qs = Perro.objects.get(id=id)
	context ={
		'perro':qs
	}
	return render(request,'Perros/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = PerroForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Perros:Lista')
	else:
		form = PerroForm()

	return render(request,'Perros/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Perro.objects.get(id=id)
	if request.method == 'GET':
		form = PerroForm(instance = qs)
	else:
		form = PerroForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Perros:Lista')
	return render(request,'Perros/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Perro.objects.get(id=id)
	qs.delete()
	return redirect('Perros:Lista')