from rest_framework import serializers
from apps.Caballos.models import Caballo

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Caballo
		fields = [
			"nombre",
			"raza",
			"hogar",
			"edad",
			"altura",
			"ancho",
			"date",

		]
			
			