from django.contrib import admin
from django.urls import path
from apps.Modelo.views import Template1,Template2,Template3,Template4,Template5,Template6,Template7,Template8,Template9,Template10,Index
from apps.Modelo.views import ListaAPIView,CrearAPIView

app_name='Modelo'
urlpatterns = [
    path('',Index,name='Index'),
    path('ListaAPIView/',ListaAPIView.as_view(),name="ListaAPIView"),
    path('CrearAPIView/',CrearAPIView.as_view(),name="CrearAPIView"),
    path('template1/',Template1,name='template1'),
    path('template2/',Template2,name='template2'),
    path('template3/',Template3,name='template3'),
    path('template4/',Template4,name='template4'),
    path('template5/',Template5,name='template5'),
    path('template6/',Template6,name='template6'),
    path('template7/',Template7,name='template7'),
    path('template8/',Template8,name='template8'),
    path('template9/',Template9,name='template9'),
    path('template10/',Template10,name='template10'),
]