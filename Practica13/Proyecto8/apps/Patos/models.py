from django.db import models

# Create your models here.
class Pato(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)
	habitad = models.CharField(max_length=30)	
	color = models.CharField(max_length=30)
	edad = models.IntegerField()
	peso = models.FloatField()
	date = models.DateField()

	def __str__(self):
		return self.nombre