from rest_framework import serializers
from apps.Patos.models import Pato

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Pato
		fields = [
			"nombre",
			"raza",
			"habitad",
			"color",
			"edad",
			"peso",
			"date",

		]
