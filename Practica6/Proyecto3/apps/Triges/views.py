from django.shortcuts import render
from apps.Triges.models import Tigre

# Create your views here.
def Lista(request):
	qs = Tigre.objects.all()
	context = {
		'tigres' : qs
	}
	return render(request,'Tigres/lista.html',context)

def Detalles(request,id):
	qs = Tigre.objects.get(id=id)
	context = {
		'tigre' : qs
	}
	return render(request,'Tigres/detalles.html',context)