from django.shortcuts import render
from apps.Perros.models import Perro

# Create your views here.
def Lista(request):
	qs = Perro.objects.all()
	context ={
		'perros':qs
	}
	return render(request,'Perros/lista.html',context)

def Detalles(request,id):
	qs = Perro.objects.get(id=id)
	context ={
		'perro':qs
	}
	return render(request,'Perros/detalles.html',context)