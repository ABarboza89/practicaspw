from django.shortcuts import render
from apps.Caballos.models import Caballo

# Create your views here.
def Lista(request):
	qs = Caballo.objects.all()
	context = {
		'caballos': qs
	}
	return render(request,'Caballo/lista.html',context)

def Detalles(request,id):
	qs = Caballo.objects.get(id=id)
	context = {
		'caballo': qs
	}
	return render(request,'Caballo/detalles.html',context)