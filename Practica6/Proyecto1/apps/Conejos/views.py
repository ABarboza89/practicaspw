from django.shortcuts import render
from apps.Conejos.models import Conejo

# Create your views here.
def Lista(request):
	qs = Conejo.objects.all()	
	context = {
		"conejos": qs
	}
	return render(request,"Conejo/lista.html",context)

def Detalles(request,id):
	qs = Conejo.objects.get(id=id)	
	context = {
		"conejo": qs
	}
	return render(request,"Conejo/detalles.html",context)