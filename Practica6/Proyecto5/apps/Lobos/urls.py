from django.contrib import admin
from django.urls import path
from apps.Lobos.views import Lista,Detalles

app_name='Lobos'
urlpatterns = [
    path('lista/',Lista,name='Lista'),
    path('detalles/<int:id>',Detalles,name='Detalles'),
]