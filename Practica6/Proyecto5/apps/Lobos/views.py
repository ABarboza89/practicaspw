from django.shortcuts import render
from apps.Lobos.models import Lobo

# Create your views here.
def Lista(request):
	qs = Lobo.objects.all()
	context ={
		'lobos':qs
	}
	return render(request,'Lobos/lista.html',context)

def Detalles(request,id):
	qs = Lobo.objects.get(id=id)
	context ={
		'lobo':qs
	}
	return render(request,'Lobos/detalles.html',context)