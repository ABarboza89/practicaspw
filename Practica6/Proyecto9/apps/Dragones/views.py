from django.shortcuts import render
from apps.Dragones.models import Dragon

# Create your views here.
def Lista(request):
	qs = Dragon.objects.all()
	context ={
		'dragones':qs
	}
	return render(request,'Dragones/lista.html',context)

def Detalles(request,id):
	qs = Dragon.objects.get(id=id)
	context ={
		'dragon':qs
	}
	return render(request,'Dragones/detalles.html',context)