from django.shortcuts import render
from apps.Aguilas.models import Aguila

# Create your views here.
def Lista(request):
	qs = Aguila.objects.all()
	context ={
		'aguilas':qs
	}
	return render(request,'Aguilas/lista.html',context)

def Detalles(request,id):
	qs = Aguila.objects.get(id=id)
	context ={
		'aguila':qs
	}
	return render(request,'Aguilas/detalles.html',context)
