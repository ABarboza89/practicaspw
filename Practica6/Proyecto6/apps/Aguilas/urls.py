from django.contrib import admin
from django.urls import path
from apps.Aguilas.views import Lista,Detalles

app_name='Aguilas'
urlpatterns = [
    path('lista/',Lista,name='Lista'),
    path('detalles/<int:id>',Detalles,name='Detalles'),
]