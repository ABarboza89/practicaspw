from django.contrib import admin
from django.urls import path
from apps.Patos.views import Lista,Detalles

app_name='Patos'
urlpatterns = [
    path('lista/',Lista,name='Lista'),
    path('detalles/<int:id>',Detalles,name='Detalles'),
]