from django.contrib import admin
from django.urls import path
from apps.Gatos.views import Lista,Detalles

app_name='Gatos'
urlpatterns = [
    path('lista/',Lista,name='Lista'),
    path('detalles/<int:id>',Detalles,name='Detalles'),
]