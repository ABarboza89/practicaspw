from django.shortcuts import render
from apps.Gatos.models import Gato

# Create your views here.
def Lista(request):
	qs = Gato.objects.all()
	context ={
		'gatos':qs
	}
	return render(request,'Gatos/lista.html',context)

def Detalles(request,id):
	qs = Gato.objects.get(id=id)
	context ={
		'gato':qs
	}
	return render(request,'Gatos/detalles.html',context)