from rest_framework import serializers
from apps.Patos.models import Pato
from django.contrib.auth.models import User

class ApiUser(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
		]
			

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Pato
		fields = [
			"nombre",
			"raza",
			"habitad",
			"color",
			"edad",
			"peso",
			"date",

		]
