from django.contrib import admin
from django.urls import path,include,re_path
from apps.Usuarios.views import UsuarioRegistro

app_name='Usuarios'
urlpatterns = [
    path('registrar/',UsuarioRegistro.as_view(),name='Usuario_Registrar'),
]