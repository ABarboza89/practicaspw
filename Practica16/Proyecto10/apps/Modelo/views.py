from django.shortcuts import render
from apps.Modelo.serializer import ApiReadOnly,ApiUser
from rest_framework.generics import ListAPIView,CreateAPIView
from apps.Caballos.models import Caballo 
from django.contrib.auth.models import User

# Create your views here.

class UserAPICreate(CreateAPIView):
    serializer_class = ApiUser

    def perform_create(self,serializer):
        serializer.save()

class UserAPIView(ListAPIView):
    serializer_class = ApiUser

    def get_queryset(self,*args,**kwargs):
        return User.objects.all()

class CrearAPIView(CreateAPIView):
    serializer_class = ApiReadOnly

    def perform_create(self,serializer):
        serializer.save()

class ListaAPIView(ListAPIView):
    serializer_class = ApiReadOnly

    def get_queryset(self,*args,**kwargs):
        return Caballo.objects.all()
        
def Index(request):
    return render(request,'index.html')
    
def Template1(request):
    context = {
        'message': "Soy el Template 1 - Proyecto 10",
    }
    return render(request, 'Modelo/template1.html', context)

def Template2(request):
    context = {
        'message': "Soy el Template 2 - Proyecto 10",
    }
    return render(request, 'Modelo/template2.html', context)

def Template3(request):
    context = {
        'message': "Soy el Template 3 - Proyecto 10",
    }
    return render(request, 'Modelo/template3.html', context)

def Template4(request):
    context = {
        'message': "Soy el Template 4 - Proyecto 10",
    }
    return render(request, 'Modelo/template4.html', context)

def Template5(request):
    context = {
        'message': "Soy el Template 5 - Proyecto 10",
    }
    return render(request, 'Modelo/template5.html', context)

def Template6(request):
    context = {
        'message': "Soy el Template 6 - Proyecto 10",
    }
    return render(request, 'Modelo/template6.html', context)

def Template7(request):
    context = {
        'message': "Soy el Template 7 - Proyecto 10",
    }
    return render(request, 'Modelo/template7.html', context)

def Template8(request):
    context = {
        'message': "Soy el Template 8 - Proyecto 10",
    }
    return render(request, 'Modelo/template8.html', context)

def Template9(request):
    context = {
        'message': "Soy el Template 9 - Proyecto 10",
    }
    return render(request, 'Modelo/template9.html', context)

def Template10(request):
    context = {
        'message': "Soy el Template 10 - Proyecto 10",
    }
    return render(request, 'Modelo/template10.html', context)