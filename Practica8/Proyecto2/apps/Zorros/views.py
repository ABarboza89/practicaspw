from django.shortcuts import render
from apps.Zorros.models import Zorro
from apps.Zorros.forms import ZorroForm

# Create your views here.
def Lista(request):
	qs = Zorro.objects.all()
	context = {
		'zorros': qs
	}
	return render(request,'Zorro/lista.html',context)

def Detalles(request,id):
	qs = Zorro.objects.get(id=id)
	context = {
		'zorro': qs
	}
	return render(request,'Zorro/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = ZorroForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Zorros:Lista')
	else:
		form = ZorroForm()

	return render(request,'Zorro/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Zorro.objects.get(id=id)
	if request.method == 'GET':
		form = ZorroForm(instance = qs)
	else:
		form = ZorroForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Zorros:Lista')
	return render(request,'Zorro/crear.html',{'form':form})