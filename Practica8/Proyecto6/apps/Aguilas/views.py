from django.shortcuts import render
from apps.Aguilas.models import Aguila
from apps.Aguilas.forms import AguilaForm
# Create your views here.
def Lista(request):
	qs = Aguila.objects.all()
	context ={
		'aguilas':qs
	}
	return render(request,'Aguilas/lista.html',context)

def Detalles(request,id):
	qs = Aguila.objects.get(id=id)
	context ={
		'aguila':qs
	}
	return render(request,'Aguilas/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = AguilaForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Aguilas:Lista')
	else:
		form = AguilaForm()

	return render(request,'Aguilas/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Aguila.objects.get(id=id)
	if request.method == 'GET':
		form = AguilaForm(instance = qs)
	else:
		form = AguilaForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Aguilas:Lista')
	return render(request,'Aguilas/crear.html',{'form':form})