from django.db import models

# Create your models here.
class Conejo(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)	
	edad = models.IntegerField()
	size = models.IntegerField()
	date = models.DateField()

	def __str__(self):
		return self.nombre